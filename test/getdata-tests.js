var assert = require('assert');
var sut=require('../routes/data');

describe('getdata', function() {
	describe('when invoked without data', function() {
		it("should repond Data is null", function(done) {
			var fakereq={body: {}};
			var fakeres={send: function(str){
				assert.equal(str, 'Data is null');
				done();
			}};// end fakeres

			sut(fakereq, fakeres);
			
		}); //end if data it
	})//end dscribe without data

	describe('getdata', function() {
		describe('when invoked with data', function() {
			it("should repond Data is OK!", function(done) {
				var fakereq={body: {data: 'data'}};
				var fakeres={send: function(str){
					assert.equal(str, 'OK!');
					done();
				}}//end fakeres

				sut(fakereq, fakeres);
			});//end it
		});//end describe 2
	}); //end describe myfunct
});//end 