var assert = require('assert');
var sut=require('../routes/login');

describe('login', function() {

	describe('when invoked without username and password', function() {
		it("should repond Login Invalid", function(done) {
			var fakereq={body: {}};
			var fakeres={send: function(str){
				assert.equal(str, 'Login Invalid');
				done();
			}};//end fake res

			sut(fakereq, fakeres);
			
		}); //end it - Login invalud
	}); //end username and password

	//username/pass succeeds
	describe('when invoked with username and password', function() {
		it("should repond OK", function(done) {
			var fakereq={body: {"username" : "test", "password": "test"}};
			var fakeres={send: function(str){
				assert.equal(str, 'OK');
				done();
			}};//end fake res

			sut(fakereq, fakeres);
			
		}); //end it - Login invalud
	}); //end username and password


}); //end login