# Mocha Testing

Test environment for Node based javascript apps and APIs to provide easy unit testing. 

Master Branch tests two basic functions, Login and GetData and their response to SUCCESS and FAIL verification. 

This can be used for a basic start for other java based applications written in Html5, AngularJS and Ionic. Just pull the respective branch you are interested in and begin unit testing.  For each seperate application the developer has the option to  employ either Test Driven Design (TDD) or Behavior Driven Design (BDD) according to preference.

## Branches
	master
	html5
	angular
	ionic

## Questions?

If you have any questions, feel free to contact me  <cooper@massideation.com>.